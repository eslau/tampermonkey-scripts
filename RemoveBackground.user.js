// ==UserScript==
// @name         RemoveBackground
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @description  Stops GSAC modal dialog from blocking scrolling / copy paste, etc (aui-blanket)
// @author       Evan Slaughter <evanslaughter@gmail.com>
// @include      https://jira.example.com/secure/Dashboard.jspa
// @include      https://jira.example.com/secure/Dashboard.jspa*
// @include      https://jira.example.com/issues/?filter=*
// @include      https://jira.example.com/issues/?jql=*
// @include      https://jira.example.com/browse/*
// @downloadURL  https://bitbucket.org/eslau/tampermonkey-scripts/raw/main/RemoveBackground.user.js
// @updateURL    https://bitbucket.org/eslau/tampermonkey-scripts/raw/main/RemoveBackground.user.js
// @noframes
// ==/UserScript==

(function() {
    'use strict';

    $(document).ready(function() {
        // Select the node that will be observed for mutations (body Jira node since the blanket spawns under this. )
        const targetNode = document.getElementById('jira');

        const config = { attributes: false, childList: true, subtree: false };

        // Callback function to execute when mutations are observed
        const callback = function(mutationsList, observer) {
            for(let mutation of mutationsList) {
                if (mutation.type === 'childList') {
                    for(let node of mutation.addedNodes) {
                        if (!(node instanceof HTMLElement)) continue;

                        if (node.matches('div[class*="aui-blanket"]')) {
                            node.remove();
                            console.log('blanket killed');
                        }
                    }
                }
            }
        };

        // Create an observer instance linked to the callback function
        const observer = new MutationObserver(callback);
        observer.observe(targetNode, config);

        //observer.disconnect();
    });
})();

